# DEV-TASKS - Monitoring tasks performed by developers

## Domain

This application obtains all tasks performed by developers from the management tools, used by the team, and presents the daily performance of each developer.

## Entities

- developers
- management tools
- projects
- tasks

## Domain Model

- Gets tasks performed from management tools and show total per day and per month for each developer.
  - To get completed tasks, it is necessary:
    - to have management tool's connection data.
    - a list of projects
    - a list of developers

## Implementation: requirements,Architeture, Integration, Development, Ingestion, Pipeline, AAA; eMonitoring

## Summary

## Installation

### How to install

#### Clone

### How to Run

#### Tests

#### Linter

### How to use

#### API

## Tutorial

### Goals

## How to Contribute

## help

### FAQ

### Release/Fix

- Date: 27/01/2023
- Version: 0.1.0
- Description: DEV-TASKS - Monitoring tasks performed by developers
- Author: José Ricardo da Silva Ferreira
